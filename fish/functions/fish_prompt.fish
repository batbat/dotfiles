function fish_prompt --description 'Screen Savvy prompt'
    if test -z "$WINDOW"
        printf '%s[%s]%s[%s]%s-> ' (set_color EBCB8B) $USER (set_color 81A1C1) (prompt_pwd) (set_color BF616A)
    else
        printf '%s%s@%s%s%s(%s)%s%s%s> ' (set_color yellow) $USER (set_color purple) (prompt_hostname) (set_color white) (echo $WINDOW) (set_color $fish_color_cwd) (prompt_pwd) (set_color normal)
    end
end
