" This line makes pacman-installed global Arch Linux vim packages work.
source /usr/share/nvim/archlinux.vim

" General
	set number relativenumber

" Enable autocompletion:
	set wildmode=longest,list,full

" Disables auto comments
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
