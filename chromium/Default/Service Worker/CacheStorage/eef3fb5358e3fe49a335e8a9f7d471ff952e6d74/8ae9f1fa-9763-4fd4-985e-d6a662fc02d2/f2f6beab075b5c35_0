0\r�m��   "   �x��    https://matrixcalc.org/en/slu.html<!DOCTYPE html>
<html lang="en" dir="ltr" data-version-tag="?20210202T001643Z">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="application-name" content="matrixcalc" />
  <meta name="theme-color" content="#2B5797" />

  <!--[if lte IE 8]>
    <script src="../ie8.js"></script>
  <![endif]-->
  <!--<script defer src="../i18n-en.js?20210202T001643Z"></script>-->
  <link rel="preload" href="../i18n-en.json?20210202T001643Z" as="fetch" crossorigin="anonymous" />
  <script defer src="../mjs.js?20210202T001643Z"></script>

  <link rel="alternate" hreflang="ar" href="../ar/slu.html" />
  <link rel="alternate" hreflang="bg" href="../bg/slu.html" />
  <link rel="alternate" hreflang="ca" href="../ca/slu.html" />
  <link rel="alternate" hreflang="cs" href="../cs/slu.html" />
  <link rel="alternate" hreflang="de" href="../de/slu.html" />
  <link rel="alternate" hreflang="en" href="../en/slu.html" />
  <link rel="alternate" hreflang="es" href="../es/slu.html" />
  <link rel="alternate" hreflang="fa" href="../fa/slu.html" />
  <link rel="alternate" hreflang="fr" href="../fr/slu.html" />
  <link rel="alternate" hreflang="gl" href="../gl/slu.html" />
  <link rel="alternate" hreflang="it" href="../it/slu.html" />
  <link rel="alternate" hreflang="ja" href="../ja/slu.html" />
  <link rel="alternate" hreflang="ko" href="../ko/slu.html" />
  <link rel="alternate" hreflang="mk" href="../mk/slu.html" />
  <link rel="alternate" hreflang="nl" href="../nl/slu.html" />
  <link rel="alternate" hreflang="no" href="../no/slu.html" />
  <link rel="alternate" hreflang="pl" href="../pl/slu.html" />
  <link rel="alternate" hreflang="pt" href="../pt/slu.html" />
  <link rel="alternate" hreflang="ro" href="../ro/slu.html" />
  <link rel="alternate" hreflang="ru" href="../slu.html" />
  <link rel="alternate" hreflang="sk" href="../sk/slu.html" />
  <link rel="alternate" hreflang="tr" href="../tr/slu.html" />
  <link rel="alternate" hreflang="uk" href="../uk/slu.html" />
  <link rel="alternate" hreflang="ur" href="../ur/slu.html" />
  <link rel="alternate" hreflang="vi" href="../vi/slu.html" />
  <link rel="alternate" hreflang="zh" href="../zh/slu.html" />
  <link rel="canonical" href="https://matrixcalc.org/en/slu.html" />


  <link rel="manifest" href="manifest.json" />
  <link rel="icon" type="image/x-icon" sizes="16x16" href="../favicon.ico" />
  <link rel="icon" type="image/svg+xml" sizes="16x16" href="../favicon.svg" />
  <link href="../mcss.css?20210202T001643Z" rel="stylesheet" type="text/css" />

  <title>Solving Systems of linear equations</title>
  <meta name="description" content="System of linear equations calculator - solve system of linear equations step-by-step, Gaussian elimination, Cramer's rule, inverse matrix method, analysis for compatibility" />
</head>
<body>
<header id="header">
  <div id="logo"><a href="./">Matrix calculator</a><span id="happy-new-year" title="Happy New 2021 Year!">С Новым 2021 Годом!</span></div>
  <nav id="language">
    <button type="button" id="language-popup-button" class="popup-button" data-menu="language-popup" aria-haspopup="true" lang="en" title="Language"></button>
    <div id="language-popup" class="menu-dialog" role="menu">
      <a href="../ar/slu.html" hreflang="ar" title="Arabic" class="menuitem" role="menuitem"><span lang="ar" dir="rtl">العربية</span></a><a href="../bg/slu.html" hreflang="bg" title="Bulgarian" class="menuitem" role="menuitem"><span lang="bg" dir="ltr">Български</span></a><a href="../ca/slu.html" hreflang="ca" title="Catalan" class="menuitem" role="menuitem"><span lang="ca" dir="ltr">Català</span></a><a href="../cs/slu.html" hreflang="cs" title="Czech" class="menuitem" role="menuitem"><span lang="cs" dir="ltr">Čeština</span></a><a href="../de/slu.html" hreflang="de" title="German" class="menuitem" role="menuitem"><span lang="de" dir="ltr">Deutsch</span></a><a href="../en/slu.html" hreflang="en" title="English" class="menuitem" role="menuitem" aria-current="page"><span lang="en" dir="ltr">English</span></a><a href="../es/slu.html" hreflang="es" title="Spanish" class="menuitem" role="menuitem"><span lang="es" dir="ltr">Español</span></a><a href="../fa/slu.html" hreflang="fa" title="Persian" class="menuitem" role="menuitem"><span lang="fa" dir="rtl">فارسی</span></a><a href="../fr/slu.html" hreflang="fr" title="French" class="menuitem" role="menuitem"><span lang="fr" dir="ltr">Français</span></a><a href="../gl/slu.html" hreflang="gl" title="Galician" class="menuitem" role="menuitem"><span lang="gl" dir="ltr">Galego</span></a><a href="../it/slu.html" hreflang="it" title="Italian" class="menuitem" role="menuitem"><span lang="it" dir="ltr">Italiano</span></a><a href="../ja/slu.html" hreflang="ja" title="Japanese" class="menuitem" role="menuitem"><span lang="ja" dir="ltr">日本語</span></a><a href="../ko/slu.html" hreflang="ko" title="Korean" class="menuitem" role="menuitem"><span lang="ko" dir="ltr">한국어</span></a><a href="../mk/slu.html" hreflang="mk" title="Macedonian" class="menuitem" role="menuitem"><span lang="mk" dir="ltr">Македонски</span></a><a href="../nl/slu.html" hreflang="nl" title="Dutch" class="menuitem" role="menuitem"><span lang="nl" dir="ltr">Nederlands</span></a><a href="../no/slu.html" hreflang="no" title="Norwegian" class="menuitem" role="menuitem"><span lang="no" dir="ltr">Norsk</span></a><a href="../pl/slu.html" hreflang="pl" title="Polish" class="menuitem" role="menuitem"><span lang="pl" dir="ltr">Polski</span></a><a href="../pt/slu.html" hreflang="pt" title="Portuguese" class="menuitem" role="menuitem"><span lang="pt" dir="ltr">Português</span></a><a href="../ro/slu.html" hreflang="ro" title="Romanian" class="menuitem" role="menuitem"><span lang="ro" dir="ltr">Română</span></a><a href="../slu.html" hreflang="ru" title="Russian" class="menuitem" role="menuitem"><span lang="ru" dir="ltr">Русский</span></a><a href="../sk/slu.html" hreflang="sk" title="Slovak" class="menuitem" role="menuitem"><span lang="sk" dir="ltr">Slovenčina</span></a><a href="../tr/slu.html" hreflang="tr" title="Turkish" class="menuitem" role="menuitem"><span lang="tr" dir="ltr">Türkçe</span></a><a href="../uk/slu.html" hreflang="uk" title="Ukrainian" class="menuitem" role="menuitem"><span lang="uk" dir="ltr">Українська</span></a><a href="../ur/slu.html" hreflang="ur" title="Urdu" class="menuitem" role="menuitem"><span lang="ur" dir="rtl">اردو</span></a><a href="../vi/slu.html" hreflang="vi" title="Vietnamese" class="menuitem" role="menuitem"><span lang="vi" dir="ltr">Tiếng Việt</span></a><a href="../zh/slu.html" hreflang="zh" title="Chinese" class="menuitem" role="menuitem"><span lang="zh" dir="ltr">中文(繁體)</span></a>
    </div>
  </nav>
</header>
<div id="snow"></div>
<style>
#snow {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    pointer-events: none;
    z-index: 1000;
}
</style>

<div id="bigtable">
  <div id="left">
    <nav class="menu"><!--
    --><a href="./">Matrix calculator</a><!--
    --><a href="slu.html">Solving systems of linear equations</a><!--
    --><a href="det.html">Determinant calculator</a><!--
    --><a href="vectors.html">Eigenvalues calculator</a><!--
    --><a href="ex.html" class="langRuOnly" hreflang="ru">Examples of solvings</a><!--
    --><a href="https://en.wikipedia.org/wiki/Matrix_(mathematics)">Wikipedia:Matrices</a><!--
    --></nav>

    <aside>
      <div class="ads-container">
        <div class="adsbygoogle-container">
          <!-- v1 -->
          <ins class="adsbygoogle"
               data-ad-client="ca-pub-2164951084061218"
               data-ad-slot="9949413392"
               data-ad-format="auto"
               data-full-width-responsive="true"></ins>
        </div>
        <div>
          <button type="button" class="toggle-ads-button">
            <span>Hide Ads</span>
            <span hidden>Show Ads</span>
          </button>
        </div>
      </div>
    </aside>

  </div>
<div id="right">
<main>
<h1>Solving systems of linear equations</h1>
<p>This calculator solves <a href="https://en.wikipedia.org/wiki/System_of_linear_equations">Systems of Linear Equations</a> using <a href="https://en.wikipedia.org/wiki/Gaussian_elimination">Gaussian Elimination Method</a>, <a href="https://www.mathportal.org/algebra/solving-system-of-linear-equations/inverse-matrix-method.php">Inverse Matrix Method</a>, or <a href="https://en.wikipedia.org/wiki/System_of_linear_equations#Cramer%27s_rule">Cramer's rule</a>. Also you can compute a number of solutions in a system of linear equations (analyse the compatibility) using <a href="https://en.wikipedia.org/wiki/Rouché–Capelli_theorem">Rouché–Capelli theorem</a>.</p>
<p>Enter coefficients of your system into the input fields. Leave cells empty for variables, which do not participate in your equations. To input fractions use <code>/</code>: <code>1/3</code>.</p>
<form class="main-form">
  <fieldset>
    <legend>System of equations:</legend>
    <div class="insert-table" data-id="A" data-sizes="4x5" data-type="system"></div>
  </fieldset>
  <div role="group" class="bbox slu-buttons">
    <div><button type="button" class="expression-button" data-expression="analyse-compatibility A">Test For Compatibility</button></div>
    <div><button type="button" class="expression-button" data-expression="solve-using-Cramer's-rule A">Solve by Cramer's rule</button></div>
    <div><button type="button" class="expression-button" data-expression="solve-using-inverse-matrix-method A">Solve using the inverse matrix</button></div>
    <div><button type="button" class="expression-button" data-expression="solve-using-Montante-method A">Montante's method (Bareiss algorithm)</button></div>
    <div><button type="button" class="expression-button" data-expression="solve-using-Gaussian-elimination A">Solve by Gaussian elimination</button></div>
    <div><button type="button" class="expression-button" data-expression="solve-using-Gauss-Jordan-elimination A">Solve by Gauss–Jordan elimination</button></div>
  </div>
</form>

<div class="line">
    <div class="decimal-fraction-digits-controls">
      <input type="checkbox" id="decfraccheckbox" /><label for="decfraccheckbox">Display decimals</label><span hidden id="frdigitsspan"><span>, </span><label for="frdigits">number of significant digits: </label><input type="number" min="1" step="1" id="frdigits" value="3" /></span>
    </div>
    <div>
      <button type="button" class="clear-all-button">Clean</button>
    </div>
</div>
<section id="resdiv" aria-live="polite" aria-label="Results"></section>

<ul>
<li>
  <div class="input-example-link-container"><a href="#">Show how to input the following system:</a></div>
  <code class="input-example-code" translate="no" dir="ltr">
    <span>2x-2y+z=-3</span>
    <span>x+3y-2z=1</span>
    <span>3x-y-z=2</span>
  </code>
</li>
</ul>

<section id="advices" aria-label="Advices">
  <ul>
    <li>Leave extra cells <i>empty</i> to enter non-square matrices.</li>
    <li>You can use decimal (finite and periodic) fractions: <code dir="ltr">1/3</code>, <code dir="ltr">3.14</code>, <code dir="ltr">-1.3(56)</code>, or <code dir="ltr">1.2e-4</code>; or arithmetic expressions: <code dir="ltr">2/3+3*(10-4)</code>, <code dir="ltr">(1+x)/y^2</code>, <code dir="ltr">2^0.5 (=<math><msqrt><mn>2</mn></msqrt></math>)</code>, <code dir="ltr">2^(1/3)</code>, <code dir="ltr">2^n</code>, <code dir="ltr">sin(phi)</code>, or <code dir="ltr">cos(3.142rad)</code>.</li>
    <li>Use <kbd>↵ Enter</kbd>, <kbd>Space</kbd>, <kbd>←</kbd>, <kbd>→</kbd>, <kbd>↑</kbd>, <kbd>↓</kbd>, <kbd>⌫</kbd>, and <kbd>Delete</kbd> to navigate between cells, <kbd class="ctrl"><span>Ctrl</span><span>⌘ Cmd</span></kbd>+<kbd>C</kbd>/<kbd class="ctrl"><span>Ctrl</span><span>⌘ Cmd</span></kbd>+<kbd>V</kbd> to copy/paste matrices.</li>
    <li><a href="https://en.wikipedia.org/wiki/Drag_and_drop">Drag-and-drop</a> matrices from the results, or even from/to a text editor.</li>
    <li>To learn more about matrices use <a href="https://en.wikipedia.org/wiki/Matrix_(mathematics)">Wikipedia</a>.</li>
  </ul>
</section>

</main>
<div class="from-cookie" hidden></div>
<div hidden>
  <span id="i18n-buttons-clear">Clean</span>
  <span id="i18n-buttons-cells">Cells</span>
  <span id="i18n-or">or</span>
  <span id="i18n-appButtons-share">Share</span>
  <span id="i18n-buttons-insertIn">Insert in</span>
  <span id="i18n-use-decimal-keyboard-on-mobile-phones"></span>
</div>

<footer id="footer">
  <address>
    <a rel="nofollow author" href="mailto:matri-tri-ca@yandex.ru">matri-tri-ca@yandex.ru</a>
  </address>
  <section id="thanks" lang="en" dir="ltr">
    <h3>Thanks to:</h3>
    <ul>
      <li>Philip Petrov (<a href="https://cphpvb.net" hreflang="bg">https://cphpvb.net</a>) for Bulgarian translation</li>
      <li>Manuel Rial Costa for Galego translation</li>
      <li>Shio Kun for Chinese translation</li>
      <li><a href="https://im-pmf-en.weebly.com/petar-sokoloski.html" hreflang="en">Petar Sokoloski</a> for Macedonian translation</li>
      <li>Duy Thúc Trần for Vietnamese translation</li>
      <li><a href="https://www.twitter.com/RKursatV" hreflang="en">Rıfkı Kürşat Vuruşan</a> for Turkish translation</li>
      <li>Ousama Malouf and Yaseen Ibrahim for Arabic translation</li>
      <li><a href="https://marcel-artz.de" hreflang="de">Marcel Artz</a> - improving of the German translation</li>
      <li><a href="https://matricesydeterminantes.com" lang="es" hreflang="es" title="teoría relacionada con matrices y determinantes">Marc Gisbert Juàrez</a> - fixing the translation into Catalan</li>
    </ul>
  </section>

</footer>

</div>
</div>

</body>
</html>
�A�Eo��   6�a�7      

GET�8�  "
accept-rangesbytes" 
access-control-allow-origin*"!
cache-controlpublic, no-cache"
content-encodinggzip"
content-length4156"�4
content-security-policy�4default-src 'unsafe-inline' 'unsafe-eval' 'self' data: blob: ws://*.hypercomments.com http://matrixcalc.org http://*.googleapis.com http://*.googlesyndication.com http://*.googletagservices.com http://*.doubleclick.net http://*.gstatic.com http://*.wp.com http://*.gravatar.com http://counter.yadro.ru http://*.hypercomments.com http://*.yandex.ru http://*.yastatic.net http://*.megaflowers.ru http://*.vk.me http://*.youtube.com http://*.ytimg.com wss://*.hypercomments.com https://matrixcalc.org https://*.googleapis.com https://*.googlesyndication.com https://*.googletagservices.com https://*.googleadservices.com https://*.doubleclick.net https://*.gstatic.com https://*.wp.com https://*.gravatar.com https://counter.yadro.ru https://adservice.google.ad https://adservice.google.ae https://adservice.google.al https://adservice.google.am https://adservice.google.as https://adservice.google.at https://adservice.google.az https://adservice.google.ba https://adservice.google.be https://adservice.google.bf https://adservice.google.bg https://adservice.google.bi https://adservice.google.bj https://adservice.google.bs https://adservice.google.bt https://adservice.google.by https://adservice.google.ca https://adservice.google.cat https://adservice.google.cd https://adservice.google.cf https://adservice.google.cg https://adservice.google.ch https://adservice.google.ci https://adservice.google.cl https://adservice.google.cm https://adservice.google.cn https://adservice.google.co.ao https://adservice.google.co.bw https://adservice.google.co.ck https://adservice.google.co.cr https://adservice.google.co.id https://adservice.google.co.il https://adservice.google.co.in https://adservice.google.co.jp https://adservice.google.co.ke https://adservice.google.co.kr https://adservice.google.co.ls https://adservice.google.co.ma https://adservice.google.co.mz https://adservice.google.co.nz https://adservice.google.co.th https://adservice.google.co.tz https://adservice.google.co.ug https://adservice.google.co.uk https://adservice.google.co.uz https://adservice.google.co.ve https://adservice.google.co.vi https://adservice.google.co.za https://adservice.google.co.zm https://adservice.google.co.zw https://adservice.google.com https://adservice.google.com.af https://adservice.google.com.ag https://adservice.google.com.ai https://adservice.google.com.ar https://adservice.google.com.au https://adservice.google.com.bd https://adservice.google.com.bh https://adservice.google.com.bn https://adservice.google.com.bo https://adservice.google.com.br https://adservice.google.com.bz https://adservice.google.com.co https://adservice.google.com.cu https://adservice.google.com.cy https://adservice.google.com.do https://adservice.google.com.ec https://adservice.google.com.eg https://adservice.google.com.et https://adservice.google.com.fj https://adservice.google.com.gh https://adservice.google.com.gi https://adservice.google.com.gt https://adservice.google.com.hk https://adservice.google.com.jm https://adservice.google.com.kh https://adservice.google.com.kw https://adservice.google.com.lb https://adservice.google.com.ly https://adservice.google.com.mm https://adservice.google.com.mt https://adservice.google.com.mx https://adservice.google.com.my https://adservice.google.com.na https://adservice.google.com.nf https://adservice.google.com.ng https://adservice.google.com.ni https://adservice.google.com.np https://adservice.google.com.om https://adservice.google.com.pa https://adservice.google.com.pe https://adservice.google.com.pg https://adservice.google.com.ph https://adservice.google.com.pk https://adservice.google.com.pr https://adservice.google.com.py https://adservice.google.com.qa https://adservice.google.com.sa https://adservice.google.com.sb https://adservice.google.com.sg https://adservice.google.com.sl https://adservice.google.com.sv https://adservice.google.com.tj https://adservice.google.com.tr https://adservice.google.com.tw https://adservice.google.com.ua https://adservice.google.com.uy https://adservice.google.com.vc https://adservice.google.com.vn https://adservice.google.cv https://adservice.google.cz https://adservice.google.de https://adservice.google.dj https://adservice.google.dk https://adservice.google.dm https://adservice.google.dz https://adservice.google.ee https://adservice.google.es https://adservice.google.fi https://adservice.google.fm https://adservice.google.fr https://adservice.google.ga https://adservice.google.ge https://adservice.google.gg https://adservice.google.gl https://adservice.google.gm https://adservice.google.gp https://adservice.google.gr https://adservice.google.gy https://adservice.google.hn https://adservice.google.hr https://adservice.google.ht https://adservice.google.hu https://adservice.google.ie https://adservice.google.im https://adservice.google.iq https://adservice.google.is https://adservice.google.it https://adservice.google.je https://adservice.google.jo https://adservice.google.kg https://adservice.google.ki https://adservice.google.kz https://adservice.google.la https://adservice.google.li https://adservice.google.lk https://adservice.google.lt https://adservice.google.lu https://adservice.google.lv https://adservice.google.md https://adservice.google.me https://adservice.google.mg https://adservice.google.mk https://adservice.google.ml https://adservice.google.mn https://adservice.google.ms https://adservice.google.mu https://adservice.google.mv https://adservice.google.mw https://adservice.google.ne https://adservice.google.nl https://adservice.google.no https://adservice.google.nr https://adservice.google.nu https://adservice.google.pl https://adservice.google.pn https://adservice.google.ps https://adservice.google.pt https://adservice.google.ro https://adservice.google.rs https://adservice.google.ru https://adservice.google.rw https://adservice.google.sc https://adservice.google.se https://adservice.google.sh https://adservice.google.si https://adservice.google.sk https://adservice.google.sm https://adservice.google.sn https://adservice.google.so https://adservice.google.sr https://adservice.google.st https://adservice.google.td https://adservice.google.tg https://adservice.google.tk https://adservice.google.tl https://adservice.google.tm https://adservice.google.tn https://adservice.google.to https://adservice.google.tt https://adservice.google.vg https://adservice.google.vu https://adservice.google.ws https://*.hypercomments.com https://*.yandex.ru https://*.yastatic.net https://*.megaflowers.ru https://*.vk.me https://*.youtube.com https://*.ytimg.com https://sun9-37.userapi.com https://telegram.org https://platform.twitter.com ; frame-src *; report-uri https://matrixcalc.org/jserrors.php?csp-report=1"(
content-typetext/html; charset=utf-8"%
dateTue, 02 Feb 2021 06:55:38 GMT"$
etag"616082d-3715-5ba4f633e9de5"".
last-modifiedTue, 02 Feb 2021 00:16:53 GMT"
servernginx"
varyAccept-Encoding0�������B"https://matrixcalc.org/en/slu.htmlHPZh2`j	text/htmlrGET������e(�����p����z��Hr*�#����)���A�Eo��   ���;      