0\r�m��   4    U�    https://matrixcalc.org/i18n-en.json?20210202T001643Z{
  "menu": {
    "matrixOperations": "Matrix calculator",
    "solvingSystemsOfLinearEquations": "Solving systems of linear equations",
    "determinantCalculation": "Determinant calculator",
    "examples": "Examples of solvings",
    "eigenvalues": "Eigenvalues calculator",
    "wiki": "<a href=\"https://en.wikipedia.org/wiki/Matrix_(mathematics)\">Wikipedia:Matrices</a>"
  },
  "index": {
    "indexTitle": "Matrix calculator",
    "indexShortDescription": "Matrix operations, eigenvalues and eigenvectors, solving of systems of equations",
    "indexDescription": "Matrix addition, multiplication, inversion, determinant and rank calculation, transposing, bringing to diagonal, triangular form, exponentiation, LU Decomposition, solving of systems of linear equations with solution steps",
    "swapMatrices": "Swap matrices",
    "multiplyMatrices": "Matrix multiplication",
    "addMatrices": "Matrix addition",
    "subtractBFromA": "Matrix subtraction",
    "indexIntro1": "With help of this calculator you can: find the matrix determinant, the rank, raise the matrix to a power, find the sum and the multiplication of matrices, calculate the inverse matrix. Just type matrix elements and click the button.",
    "findDeterminant": "Find the determinant",
    "findInverse": "Find the inverse",
    "findTranspose": "Transpose",
    "findRank": "Find the rank",
    "multiplyBy": "Multiply by",
    "multiplier": "multiplier",
    "triangularMatrix": "Triangular matrix",
    "diagonalMatrix": "Diagonal matrix",
    "exponentiation": "Raise to the power of",
    "power": "power",
    "LUDecomposition": "LU-decomposition",
    "enterExpression": "Expression input field",
    "matrix": "Matrix",
    "CholeskyDecomposition": "Cholesky decomposition"
  },
  "slu": {
    "showExampleOfSystemInput": "Show how to input the following system:",
    "sluDescription": "System of linear equations calculator - solve system of linear equations step-by-step, Gaussian elimination, Cramer's rule, inverse matrix method, analysis for compatibility",
    "sluIntro": {
      "about": "This calculator solves <a href=\"https://en.wikipedia.org/wiki/System_of_linear_equations\">Systems of Linear Equations</a> using <a href=\"https://en.wikipedia.org/wiki/Gaussian_elimination\">Gaussian Elimination Method</a>, <a href=\"https://www.mathportal.org/algebra/solving-system-of-linear-equations/inverse-matrix-method.php\">Inverse Matrix Method</a>, or <a href=\"https://en.wikipedia.org/wiki/System_of_linear_equations#Cramer%27s_rule\">Cramer's rule</a>. Also you can compute a number of solutions in a system of linear equations (analyse the compatibility) using <a href=\"https://en.wikipedia.org/wiki/Rouché–Capelli_theorem\">Rouché–Capelli theorem</a>.",
      "howTo": "Enter coefficients of your system into the input fields. Leave cells empty for variables, which do not participate in your equations. To input fractions use <code>/</code>: <code>1/3</code>."
    },
    "sluTitle": "Solving Systems of linear equations",
    "testForConsistency": "Test For Compatibility",
    "solveByCrammer": "Solve by Cramer's rule",
    "solveByInverse": "Solve using the inverse matrix",
    "solveByGauss": "Solve by Gaussian elimination",
    "solveByJordanGauss": "Solve by Gauss–Jordan elimination",
    "systemOfEquations": "System of equations"
  },
  "det": {
    "detTitle": "Matrix determinant calculator",
    "detDescription": "Determinant evaluation by using row reduction to create zeros in a row/column or using the expansion by minors along a row/column step-by-step",
    "detHeader": "Determinant calculation by expanding it on a line or a column, using Laplace's formula",
    "detIntro": "This page allows to find the determinant of a matrix using <a href=\"http://mathforum.org/library/drmath/view/51968.html\">row reduction</a>, <a href=\"https://en.wikipedia.org/wiki/Laplace_expansion\">expansion by minors</a>, or <a href=\"https://en.wikipedia.org/wiki/Leibniz_formula_for_determinants\">Leibniz formula</a>.",
    "expandByColumn": "Expand along the column",
    "expandByRow": "Expand along the row",
    "obtainZerosInColumn": "Get zeros in the column",
    "obtainZerosInRow": "Get zeros in the row",
    "ruleOfSarrus": "Rule of Sarrus",
    "ruleOfTriangle": "Triangle's rule",
    "formulaOfLeibniz": "Leibniz formula",
    "methodOfGauss": "Gaussian elimination",
    "methodOfMontante": "Montante's method (Bareiss algorithm)",
    "use": "Use",
    "useFormulaOfLeibniz": "Use Leibniz formula"
  },
  "vectors": {
    "vectorsTitle": "Eigenvalues and Eigenvectors",
    "vectorsDescription": "Calculator of eigenvalues and eigenvectors",
    "vectorsHeader": "Finding of eigenvalues and eigenvectors",
    "vectorsIntro": "This calculator allows to find <a href=\"https://en.wikipedia.org/wiki/Eigenvalues_and_eigenvectors\">eigenvalues and eigenvectors</a> using the <a href=\"https://en.wikipedia.org/wiki/Characteristic_polynomial\">Characteristic polynomial</a>.",
    "vectorsFind": "Find",
    "more": "More:",
    "JordanDecomposition": "Jordan decomposition",
    "matrixExponential": "Matrix exponential",
    "findEigenvectorsOfM": "Find eigenvectors of "
  },
  "buttons": {
    "cells": "Cells",
    "displayDecimal": "Display decimals",
    "numberOfDecimalPlaces": "number of decimal places",
    "insertIn": "Insert in",
    "clear": "Clean",
    "addTableToInputMatrix": "Add a table to input a matrix",
    "removeTable": "Remove the table"
  },
  "advices": {
    "leaveExtraCellsEmpty": "Leave extra cells <i>empty</i> to enter non-square matrices.",
    "youCanUseDecimalFractions": "You can use decimal (finite and periodic) fractions: ${listOfExamples}; or arithmetic expressions: ${listOfComplexExamples}.",
    "useKeyboardKeysToNavigate": "Use ${keyboardKeysList} to navigate between cells, ${ctrlCctrlV} to copy/paste matrices.",
    "dragAndDropMatrices": "<a href=\"https://en.wikipedia.org/wiki/Drag_and_drop\">Drag-and-drop</a> matrices from the results, or even from/to a text editor.",
    "toLearnMoreUseWikipedia": "To learn more about matrices use <a href=\"https://en.wikipedia.org/wiki/Matrix_(mathematics)\">Wikipedia</a>."
  },
  "matrixMenu": {
    "showText": "Show as plain text",
    "showMathML": "Show MathML",
    "showImage": "Show as image",
    "copyToClipboard": "Copy",
    "showLaTeX": "Show LaTeX"
  },
  "sections": {
    "showComments": "Comments",
    "examples": "Examples",
    "advices": "Advices",
    "results": "Results",
    "operationsWithMatrixA": "Operation with matrix A",
    "operationsWithMatrixB": "Operation with matrix B"
  },
  "appButtons": {
    "tweet": "Tweet",
    "share": "Share",
    "hideAds": "Hide Ads",
    "showAds": "Show Ads",
    "addAppToHomescreen": "Add app to homescreen"
  },
  "add": {
    "matrixAddition": "Matrix addition",
    "matrixAdditionInfo": "The sum of two matrices is computed by adding corresponding elements of the matrices."
  },
  "multiply": {
    "matrixMultiplication": "Matrix multiplication",
    "matrixMultiplicationInfo": "<a href=\"https://en.wikipedia.org/wiki/Matrix_multiplication\">Matrix multiplication</a>: the rows of the first matrix are multiplied by the columns of the second one."
  },
  "determinant": {
    "zeroRowColumn": "If in a matrix, any row or column is 0, then the determinant of that particular matrix is 0 (<a href=\"https://en.wikipedia.org/wiki/Determinant#Properties_of_the_determinant\">Properties of the determinant</a>).",
    "theRuleOfSarrusCanBeUsedOnlyWith3x3Matrices": "The rule of Sarrus can be used only with 3×3 matrices."
  },
  "inverse": {
    "inverse2x2": "Find 2×2 matrix inverse according to the formula:",
    "inverseDetailsUsingAdjugateMatrix": "Using the adjugate matrix",
    "determinantIsEqualToZeroTheMatrixIsSingularNotInvertible": "The determinant is 0, the matrix is not <a href=\"https://en.wikipedia.org/wiki/Invertible_matrix\">invertible</a>.",
    "methodOfGaussJordan": "Gauss–Jordan elimination"
  },
  "consistency": {
    "analyseCompatibilityOfTheSystem": "Analyse consistency of the system:",
    "analyseCompatibilityIntroduction": "Apply <a href=\"https://en.wikipedia.org/wiki/Rouché–Capelli_theorem\">Rouché–Capelli theorem</a> to compute the number of solutions.",
    "theSystemIsConsistentAndItHasAUniqueSolution": "The rank of the <a href=\"https://en.wikipedia.org/wiki/Augmented_matrix\">augmented matrix</a> is equal to the rank of the <a href=\"https://en.wikipedia.org/wiki/Coefficient_matrix\">coefficient matrix</a> of the system, and is equal to the number of variables. The system is consistent and the <a href=\"https://en.wikipedia.org/wiki/Rouché–Capelli_theorem\">solution is unique</a>.",
    "theSystemIsConsistentAndItHasInfiniteNumberOfSolutions": "The rank of the <a href=\"https://en.wikipedia.org/wiki/Augmented_matrix\">augmented matrix</a> is equal to the rank of the <a href=\"https://en.wikipedia.org/wiki/Coefficient_matrix\">coefficient matrix</a> of the system, and is less than the number of variables. The system is consistent and there is an <a href=\"https://en.wikipedia.org/wiki/Rouché–Capelli_theorem\">infinite number of solutions</a>.",
    "theSystemIsInconsistent": "The rank of the <a href=\"https://en.wikipedia.org/wiki/Augmented_matrix\">augmented matrix</a> is not equal to the rank of the <a href=\"https://en.wikipedia.org/wiki/Coefficient_matrix\">coefficient matrix</a> of the system. The system is inconsistent (<a href=\"https://en.wikipedia.org/wiki/Rouché–Capelli_theorem\">has no solution</a>)."
  },
  "systems": {
    "solutionByRuleOfCramer": "Solution by <a href=\"https://en.wikipedia.org/wiki/Cramer%27s_rule\">Cramer's rule</a>",
    "solutionByInverseMatrixMethod": "Solution by <a href=\"https://www.mathportal.org/algebra/solving-system-of-linear-equations/inverse-matrix-method.php\">Inverse Matrix Method</a>",
    "solutionByGaussianElimination": "Solution by <a href=\"https://en.wikipedia.org/wiki/Gaussian_elimination\">Gaussian elimination</a>",
    "solutionByGaussJordanElimination": "Solution by <a href=\"https://en.wikipedia.org/wiki/Gaussian_elimination\">Gauss-Jordan elimination</a>",
    "solutionByMethodOfMontante": "Solution by <a href=\"https://es.wikipedia.org/wiki/Método_Montante\">Montante's Method (Bareiss algorithm)</a>",
    "forSolutionUsingCramersRuleNumberOfEquationsShouldBeEqualNumberOfVariables": "For the solution using Cramer's rule the number of equations should be equal to the number of variables.",
    "forSolutionUsingCramersRuleCoefficientMatrixShouldHaveNonZeroDeterminant": "For the solution using <a href=\"https://en.wikipedia.org/wiki/Cramer%27s_rule\">Cramer's rule</a> the coefficient matrix should have a nonzero determinant.",
    "toSolveSystemByInverseMatrixMethodNumberOfEquationsShouldBeEqualNumberOfVariables": "To solve the system by <a href=\"https://www.mathportal.org/algebra/solving-system-of-linear-equations/inverse-matrix-method.php\">Inverse Matrix Method</a> it must have the same number of equations as variables.",
    "toSolveSystemByInverseMatrixMethodCoefficientMatrixShouldHaveNonZeroDeterminant": "To solve the system by <a href=\"https://www.mathportal.org/algebra/solving-system-of-linear-equations/inverse-matrix-method.php\">Inverse Matrix Method</a> the coefficient matrix should have a nonzero determinant.",
    "convertTheAugmentedMatrixIntoTheRowEchelonForm": "Convert the <a href=\"https://en.wikipedia.org/wiki/Augmented_matrix\">augmented matrix</a> into the row echelon form:",
    "theMatrixIsInRowEchelonForm": "The matrix is in <a href=\"https://en.wikipedia.org/wiki/Row_echelon_form\">row-echelon form</a>.",
    "thereAreNoSolutions": "There are no solutions.",
    "fromEquationIFindVariable": "Find the variable ${x} from the equation ${i} of the system ${#system_1}:",
    "answer": "Answer:",
    "generalSolution": "<a href=\"https://math.stackexchange.com/questions/299870/find-the-general-form-of-the-solution-to-the-system-of-equations-below\">General Solution</a>:",
    "fundamentalSystem": "The solution set:"
  },
  "eigenvalues": {
    "findEigenvaluesFromTheCharacteristicPolynomial": "Find eigenvalues from the <a href=\"https://en.wikipedia.org/wiki/Characteristic_polynomial\">characteristic polynomial</a>:",
    "letsSolveHomogeneouseSystem": "So we have a <a href=\"https://en.wikipedia.org/wiki/System_of_linear_equations#Homogeneous_systems\">homogeneous system</a> of linear equations, we solve it by Gaussian Elimination:",
    "findEigenvectorsForEveryEigenvalue": "For every ${λ} we find its own vectors:",
    "thereAreNoRationalSolutions": "There are no rational solutions.",
    "Let": "Let",
    "byDefinition": "From the definition of the eigenvector ${v} corresponding to the eigenvalue ${λ} we have ${A*v=λ*v}",
    "equationHasNonZeroSolution": "Equation has a nonzero solution if and only if ${|(A-λI)|=0}"
  },
  "diagonalization": {
    "notEnoughRationalEigenvalues": "Not Enough Rational Eigenvalues",
    "notDiagonalizable": "The matrix is not diagonalizable, because it does not have ${n} <a href=\"https://en.wikipedia.org/wiki/Linear_independence#Evaluating_linear_independence\">linearly independent</a> eigenvectors.",
    "theDiagonalMatrixTheDiagonalEntriesAreTheEigenvalues": "The diagonal matrix (the diagonal entries are the eigenvalues - ${eigenvaluesLinks}):",
    "theMatrixWithTheEigenvectorsAsItsColumns": "The <a href=\"https://en.wikipedia.org/wiki/Modal_matrix\">matrix with the eigenvectors</a> (${eigenvectorsLinks}) as its columns:"
  },
  "errors": {
    "inputError": "Please check entered data (you can use decimal numbers and expressions such as ${listOfExamples}, ${listOfComplexExamples})",
    "matrixIsNotSquare": "The matrix is not square",
    "matricesShouldHaveSameDimensions": "Matrices must be of the same size.",
    "theNumberOfColumnsInFirstMatrixShouldEqualTheNumberOfRowsInSecond": "The number of columns in the first matrix should be equal to the number of rows in the second.",
    "divisionByZeroError": "Division by zero",
    "pleaseFillOutThisField": "Please fill out this field.",
    "operationIsNotSupported": "This operation is not supported with the specified arguments.",
    "unexpectedEndOfInputYExpected": "Error: unexpected end of input, ${y} expected",
    "unexpectedXYExpected": "Error: unexpected ${x}, ${y} expected",
    "unexpectedX": "Error: unexpected ${x}",
    "unexpectedEndOfInput": "Error: unexpected end of input",
    "matrixArgExpected": "Matrix argument is expected"
  },
  "dpnMatrixPow": {
    "define": "Define",
    "soThat": "so that the problem is to compute ${(D+N)**n}. The big, important things to note here are",
    "DAndNCommute": "${D} and ${N} commute",
    "whichEnables": "which enables the following powerful tricks: the first point lets us expand it with the binomial theorem, and the second point lets us truncate to the first few terms:"
  },
  "misc": {
    "roots": "Roots:",
    "powUsingDiagonalizationIntro": "We find a matrix in diagonal form, then the power can be found as:",
    "powUsingJordanNormalFormIntro": "We find a matrix in Jordan normal form, then the power can be found as:",
    "powOfJordanMatrix": "The power of a Jordan matrix can be found as:",
    "nthRootOfJordanMatrix": "The nth-root of a Jordan matrix can be found as:",
    "ifXisARootOfAMatrixInDiagonalFormThen": "if ${D^(1/n)} is a root of a matrix in diagonal form, then:",
    "ifXisARootOfAMatrixInJordanNormalFormThen": "if ${J^(1/n)} is a root of a matrix in Jordan normal form, then:",
    "thisMeansThatXIsANthRootOfTheMatrix": "This means, that ${P*J^(1/n)*P^-1} is one of the nth-roots of the matrix.",
    "labelTitle": "Arrow from row ${x} to row ${y}",
    "swapLabelTitle": "Two-way arrow between row ${x} and row ${y}",
    "close": "Close",
    "keyEnter": "Enter",
    "keySpace": "Space",
    "keyDelete": "Delete",
    "summaryLabel": "Details",
    "calculatingTheLogarithmOfADiagonalizableMatrix": "Calculating the logarithm of a diagonalizable matrix"
  },
  "unused": {
    "systems": {
      "augmentedMatrixOfTheSystem": "The <a href=\"https://en.wikipedia.org/wiki/Augmented_matrix\">augmented matrix</a> of the system:"
    },
    "determinant": {
      "usingSarrusRule": "Using <a href=\"https://en.wikipedia.org/wiki/Rule_of_Sarrus\">Sarrus' rule</a>"
    },
    "other": {
      "exponentIsNegative": "The exponent is negative",
      "language": "Language",
      "or": "or"
    }
  },
  "JordanDecomposition": {
    "tryToFindJordanNormalForm": "Try to *find a Jordan normal form*.",
    "findAMatrixInJordanNormalFormSimilarToOriginal": "Find a matrix in Jordan normal form, <a href=\"https://en.wikipedia.org/wiki/Matrix_similarity\">similar to original</a>",
    "solveTheCharacteristicEquationForEigenvaluesAndTheirAlgebraicMultiplicities": "Solve the characteristic equation for eigenvalues and their algebraic multiplicities:",
    "findLinearlyIndependentGeneralizedEigenvectorsForEveryEigenvalue": "Find linearly independent generalized eigenvectors for every eigenvalue:",
    "eigenvalue": "Eigenvalue",
    "algebraicMultiplicity": "algebraic multiplicity",
    "determineTheMaximalRankOfGeneralizedEigenvectors": "Determine the maximal rank of generalized eigenvectors:",
    "determineEachJordanChain": "Determine each Jordan chain:",
    "findSolutionsOfX": "Find solutions of ${X}:",
    "aBasisForTheSolutionSet": "A basis for the solution set:",
    "itIsAGeneralizedEigenvector": "It is a generalized eigenvector.",
    "itIsNotAGeneralizedEigenvector": "It is not a generalized eigenvector.",
    "generateAJordanChainForThisGeneralizedEigenvector": "Generate a Jordan Chain for this generalized eigenvector:",
    "theJordanChainsMakeBasis": "The Jordan chains of generalized eigenvectors ${links} make a linearly independent set of vectors for the eigenvalue.",
    "generalizedModalMatrix": "<a href=\"https://en.wikipedia.org/wiki/Modal_matrix#Generalized_modal_matrix\">Generalized modal matrix</a> (the columns of M are the generalized eigenvectors of selected chains in reverse order - ${links}):",
    "JordanMatrix": "A matrix J in Jordan normal form (a matrix from <a href=\"https://en.wikipedia.org/wiki/Jordan_matrix\">Jordan blocks</a> corresponding to selected chains):"
  },
  "rankDetails": {
    "start": "Find the rank of matrix by <a href=\"https://en.wikipedia.org/wiki/Elementary_matrix#Elementary_row_operations\">elementary row operations</a>. The rank of the matrix is equal to the number of nonzero rows in the matrix after reducing it to <a href=\"https://en.wikipedia.org/wiki/Row_echelon_form\">the row echelon form</a> using elementary transformations over the rows of the matrix."
  },
  "determinantDetails": {
    "start": "Convert the matrix into the <a href=\"https://en.wikipedia.org/wiki/Row_echelon_form\">row echelon form</a>. Addition operation to one of the rows of another one multiplied by some number does not change <a href=\"https://en.wikipedia.org/wiki/Determinant\">the determinant</a>. The determinant of the transformed matrix is equal to the determinant of the original one."
  },
  "inverseDetails": {
    "start": "Find the inverse matrix using <a href=\"https://en.wikipedia.org/wiki/Invertible_matrix\">elementary transformations</a>, to do this <a href=\"https://en.wikipedia.org/wiki/Gaussian_elimination\">augment</a> the identity matrix of the same size to the right:"
  },
  "eliminationDetails": {
    "pivotElement": "Pivot element:",
    "rowAddition": "Add to row ${i} row ${j}:",
    "rowSwapNegate": "Swap row ${i} and row ${j}, multiplying row ${i} by ${-1}:",
    "rowSwap": "Swap row ${i} and ${j}:",
    "rowDivision": "Divide row ${j} by ${a}:",
    "rowSubtraction": "Subtract ${a} × row ${i} from row ${j}:",
    "columnSubtraction": "Subtract from column ${j} ${a} mulitplied by column ${i}. Note: ${someDetails3}."
  },
  "methodOfMontanteDetails": {
    "determinantDetails": {
      "header": "Calculation of matrix determinant by <a href=\"https://en.wikipedia.org/wiki/Bareiss_algorithm\">Bareiss algorithm</a>",
      "start": "The determinant of the matrix is equal to the element in the last line after reducing the matrix to the row echelon form using the formula ${a_(i,j)=(a_(r,c)*a_(i,j)-a_(i,c)*a_(r,j))/p}, where ${r} and ${c} - the numbers of the row and the column of support element, and ${p} - the value of the support element in the previous step. Note: ${someDetails3}."
    }
  },
  "CholeskyDecomposition": {
    "then": "Then",
    "definition": "${A=L*L^T}, where ${L} is a lower triangular matrix with real and positive diagonal entries",
    "theMatrixIsNotSymmetric": "The matrix is not symmetric",
    "theMatrixIsSymmetric": "The matrix is symmetric",
    "matrixIsNotReal": "The matrix is not real",
    "theMatrixIsNotHermitian": "The matrix is not <a href=\"https://en.wikipedia.org/wiki/Hermitian_matrix\">Hermitian</a>",
    "theMatrixIsHermitian": "The matrix is Hermitian",
    "matrixIsNotComplex": "The matrix is not complex",
    "theMatrixIsNotPositiveDefinite": "The matrix is not positive definite",
    "sorryCannotWork": "Sorry, the calculator cannot work with such expressions"
  },
  "exponential": {
    "exponentialUsingJordanCanonicalForm": "<a href=\"https://en.wikipedia.org/wiki/Matrix_exponential#Using_the_Jordan_canonical_form\">Matrix exponential using the Jordan canonical form</a>",
    "theMatrixIsNilpotent": "The matrix is <a href=\"https://en.wikipedia.org/wiki/Nilpotent_matrix#Example_2\">nilpotent</a> as all entries on and below the main diagonal are zeros.",
    "findAMatrixInJordanNormalForm": "Find a matrix in Jordan normal form:",
    "then": "Then:",
    "findTheExponentialOfTheNilpotentMatrixN": "Find the exponential of the nilpotent matrix ${N}:",
    "findTheExponentialOfTheDiagonalMatrixD": "Find the exponential of the diagonal matrix ${D}:",
    "exponentialOfDiagonalMatrix": "Exponential can be obtained by exponentiating each entry on the main diagonal <a href=\"https://en.wikipedia.org/wiki/Matrix_exponential#Diagonalizable_case\" title=\"Matrix exponential: Diagonalizable case\">(*)</a>:"
  },
  "source": {
    "sourceTitle": "Matrix calculator: source code",
    "sourceDescription": "Source code of operation with matrices in Java and Delphi",
    "introduction": "Here you can find a source code of a simple mobile calculator (Java ME) and a simple calculator on Delphi.",
    "rosettacode": "See <a href=\"https://rosettacode.org/wiki/Category:Matrices\">https://rosettacode.org/wiki/Category:Matrices</a> for a code related to matrix inverse, determinant, rank calculation.",
    "content": "Contents:",
    "javaME": "Source code of a mobile calculator on Java ME",
    "download": "full program",
    "delphi": "Source code of a calculator on Delphi"
  },
  "polynomials": {
    "pageTitle": "Polynomials",
    "pageDescription": "Polynomial roots calculator",
    "expandAndFindRoots": "Expand (and find the roots)",
    "multiplyTwoPolynomials": "Multiply two polynomials"
  },
  "examples": {
    "pageDescription": "Usage examples of the matrix calculator",
    "item1": {
      "header": "Compute the determinant:",
      "a": "a) by expanding along the i-th row;",
      "b": "b) by expanding down the j-th column;",
      "c": "c) first obtaining zeros in the i-th row.",
      "howto": {
        "common": "For a solution you could use the page \"<a href=\"./det.html\">${menu.determinantCalculation}</a>\":",
        "a": "a) You should enter in the input field near the button \"${det.expandByRow}\" the row number - <code>1</code>. Then click this button. The solution will appear below.",
        "b": "b) You should enter in the input field near the button \"${det.expandByColumn}\" the column number - <code>2</code>. Then click this button. The solution will appear below.",
        "c": "c) You should enter in the input field near the button \"${det.obtainZerosInRow}\" the row number - <code>1</code>. Then click this button. The solution will appear below."
      }
    },
    "item2": {
      "header": "Evaluate a matrix expression to find the matrix K:",
      "howto": {
        "start": "To find the solution it is possible to use the page \"<a href=\"./\">${menu.matrixOperations}</a>\":",
        "step1": "Find on the page a <a href=\"./#add-table\">button which adds matrix input tables</a>, press it two times to get the input fields for matrices C and D.",
        "step2": "Enter the matrix A into the table \"Matrix A\", the matrix B into the table \"Matrix B\", the matrix C into the table \"Matrix C\", the matrix D into the table \"Matrix D\".",
        "step3": "Then enter the expression <code>3AB-2CD</code> into the <a href=\"./#expression\">expression input field</a> and press the button \"=\" next to the field.",
        "step4": "The result will appear below on the page."
      }
    },
    "item3": {
      "header": "A problem. The enterprise lets out three kinds of production, using raw material of three types. Charges of each type of raw material by kinds of production and stocks of raw material at the enterprise are given in the table. To define volume of output of each kind at the set stocks of raw material.",
      "rawMaterialType": "Type of raw material",
      "rawMaterialConsumptionByProductType": "Charge of raw material by kinds of production, weight/num.",
      "rawMaterialStock": "Stock of raw material, weight units",
      "howto": {
        "makeSystemOfEquations": "Let's make a system of the equations:",
        "start": "Let's use the page \"<a href=\"./slu.html\">${menu.solvingSystemsOfLinearEquations}</a>\" to find the solution:",
        "step1": "Put the coefficients of the system into the input fields.",
        "step2": "Then press the button \"${slu.solveByCrammer}.\""
      }
    }
  },
  "direction": "ltr",
  "languageName": "English",
  "externalLinks": " ",
  "keyboardKeysList": "<kbd>↵ ${misc.keyEnter}</kbd>, <kbd>${misc.keySpace}</kbd>, <kbd>←</kbd>, <kbd>→</kbd>, <kbd>↑</kbd>, <kbd>↓</kbd>, <kbd>⌫</kbd>, and <kbd>${misc.keyDelete}</kbd>",
  "ctrlCctrlV": "<kbd class=\"ctrl\"><span>Ctrl</span><span>⌘ Cmd</span></kbd>+<kbd>C</kbd>/<kbd class=\"ctrl\"><span>Ctrl</span><span>⌘ Cmd</span></kbd>+<kbd>V</kbd>",
  "determinant2x2Link": "<a href=\"https://en.wikipedia.org/wiki/Determinant#2_×_2_matrices\" title=\"Determinant: 2 × 2 matrices\">(?)</a>",
  "inverse2x2Link": "<a href=\"https://en.wikipedia.org/wiki/Invertible_matrix#Inversion_of_2_×_2_matrices\" title=\"Invertible matrix: Inversion of 2 × 2 matrices\">*</a>",
  "ruleOfSarrusLink": "<a href=\"https://en.wikipedia.org/wiki/Rule_of_Sarrus\" title=\"Rule of Sarrus\">(?)</a>",
  "ruleOfTriangleLink": "<a href=\"http://m-hikari.com/ija/ija-password-2009/ija-password5-8-2009/hajrizajIJA5-8-2009.pdf#page=3\" title=\"hajrizajIJA5-8-2009.pdf: page=3\">(?)</a>",
  "inverseDetailsUsingAdjugateMatrixLink": "<a href=\"https://en.wikipedia.org/wiki/Invertible_matrix#Analytic_solution\" title=\"Invertible matrix: Analytic solution\">(?)</a>",
  "listOfExamples": "<code dir=\"ltr\">1/3</code>, <code dir=\"ltr\">3.14</code>, <code dir=\"ltr\">-1.3(56)</code>, or <code dir=\"ltr\">1.2e-4</code>",
  "listOfComplexExamples": "<code dir=\"ltr\">2/3+3*(10-4)</code>, <code dir=\"ltr\">(1+x)/y^2</code>, <code dir=\"ltr\">2^0.5 (=<math><msqrt><mn>2</mn></msqrt></math>)</code>, <code dir=\"ltr\">2^(1/3)</code>, <code dir=\"ltr\">2^n</code>, <code dir=\"ltr\">sin(phi)</code>, or <code dir=\"ltr\">cos(3.142rad)</code>",
  "matrixRowDenotation": "<msub><mi>R</mi><mn>${i}</mn></msub>",
  "colonSpacing": "",
  "decimalSeparator": ".",
  "matrixDiagonalizationLink": "<a href=\"https://www.youtube.com/watch?v=Sf91gDhVZWU\" title=\"watch?v=Sf91gDhVZWU\">(?)</a>",
  "adjugateMatrixLink": "<a href=\"https://en.wikipedia.org/wiki/Adjugate_matrix#Definition\" title=\"Adjugate matrix: Definition\">(?)</a>",
  "identityMatrixDenotation": "I",
  "eigenvalueEquationLink": "<a href=\"https://en.wikipedia.org/wiki/Eigenvalues_and_eigenvectors#Eigenvalues_and_the_characteristic_polynomial\" title=\"Eigenvalues and eigenvectors: Eigenvalues and the characteristic polynomial\">*</a>",
  "CholeskyDecompositionLink": "<a href=\"https://en.wikipedia.org/wiki/Cholesky_decomposition\" title=\"Cholesky decomposition\">(?)</a>",
  "useTheRationalRootTestLink": "<a href=\"https://en.wikipedia.org/wiki/Rational_root_theorem#Examples\" title=\"Rational root theorem: Examples\">(?)</a>",
  "solveQuadraticEquationLink": "<a href=\"https://en.wikipedia.org/wiki/Factorization#Using_formulas_for_polynomial_roots\" title=\"Factorization: Using formulas for polynomial roots\">(?)</a>",
  "solvePalindromicEquaionLink": "<a href=\"https://en.wikipedia.org/wiki/Reciprocal_polynomial#Palindromic_and_antipalindromic_polynomials\" title=\"Reciprocal polynomial: Palindromic and antipalindromic polynomials\">(?)</a>",
  "binomialTheoremLink": "<a href=\"https://en.wikipedia.org/wiki/Binomial_theorem\" title=\"Binomial theorem\">(?)</a>",
  "solveCubicEquationLink": "<a href=\"https://en.wikipedia.org/wiki/Cubic_equation#General_cubic_formula\" title=\"Cubic equation: General cubic formula\">(?)</a>",
  "solveQuarticEquationLink": "<a href=\"https://en.wikipedia.org/wiki/Quartic_function#Descartes'_solution\" title=\"Quartic function: Descartes' solution\">(?)</a>",
  "applyDifferenceOfSquaresRuleLink": "<a href=\"https://en.wikipedia.org/wiki/Difference_of_two_squares\" title=\"Difference of two squares\">(?)</a>",
  "applyDifferenceOfCubesRuleLink": "<a href=\"https://en.wikipedia.org/wiki/Factorization#Recognizable_patterns\" title=\"Factorization: Recognizable patterns\">(?)</a>",
  "applyDifferenceOfNthPowersRuleLink": "<a href=\"https://en.wikipedia.org/wiki/Factorization#Recognizable_patterns\" title=\"Factorization: Recognizable patterns\">(?)</a>",
  "methodOfKroneckerLink": "<a href=\"https://en.wikipedia.org/wiki/Factorization_of_polynomials#Kronecker%27s_method\" title=\"Factorization of polynomials: Kronecker's method\">(?)</a>",
  "squareFreeFactorizationLink": "<a href=\"https://mathworld.wolfram.com/SquarefreeFactorization.html\" title=\"SquarefreeFactorization.html\">(?)</a>",
  "powUsingDiagonalizationLink": "<a href=\"https://en.wikipedia.org/wiki/Diagonalizable_matrix#An_application\" title=\"Diagonalizable matrix: An application\">(?)</a>",
  "powUsingJordanNormalFormLink": "<a href=\"https://en.wikipedia.org/wiki/Matrix_function#Jordan_decomposition\" title=\"Matrix function: Jordan decomposition\">(?)</a>",
  "blockOfJordanPowerFormulaLink": "<a href=\"https://en.wikipedia.org/wiki/Jordan_normal_form#Matrix_functions\" title=\"Jordan normal form: Matrix functions\">(?)</a>",
  "nthRootUsingDiagonalizationLink": "<a href=\"https://en.wikipedia.org/wiki/Square_root_of_a_matrix#By_diagonalization\" title=\"Square root of a matrix: By diagonalization\">(?)</a>",
  "JordanDecompositionLink": "<a href=\"https://en.wikipedia.org/wiki/Generalized_eigenvector#Computation_of_generalized_eigenvectors\" title=\"Generalized eigenvector: Computation of generalized eigenvectors\">(?)</a>",
  "determinantLeibnizLink": "<a href=\"https://www.youtube.com/watch?v=SIJAPMWe3rE\" title=\"watch?v=SIJAPMWe3rE\">(?)</a>",
  "exponentialOfNilpotentMatrixLink": "<a href=\"https://en.wikipedia.org/wiki/Matrix_exponential#Nilpotent_case\" title=\"Matrix exponential: Nilpotent case\">(*)</a>",
  "calculatingTheLogarithmOfADiagonalizableMatrix": "Calculating the logarithm of a diagonalizable matrix",
  "theLogarithmOfANonDiagonalizableMatrix": "The logarithm of a non-diagonalizable matrix",
  "calculatingTheLogarithmOfADiagonalizableMatrixLink": "<a href=\"https://en.wikipedia.org/wiki/Logarithm_of_a_matrix#Calculating_the_logarithm_of_a_diagonalizable_matrix\" title=\"Logarithm of a matrix: Calculating the logarithm of a diagonalizable matrix\">(?)</a>",
  "theLogarithmOfANonDiagonalizableMatrixLink": "<a href=\"https://en.wikipedia.org/wiki/Logarithm_of_a_matrix#The_logarithm_of_a_non-diagonalizable_matrix\" title=\"Logarithm of a matrix: The logarithm of a non-diagonalizable matrix\">(?)</a>",
  "numberOfSignificantDigits": "number of significant digits",
  "identityMatrix": "Identity Matrix",
  "notAllRootsHaveBeenFound": "(!) Not all roots have been found",
  "useDecimalKeybaordOnMobilePhones": "Use decimal keyboard on mobile phones"
}�A�Eo��   L2W      

GET��  " 
access-control-allow-origin*")
cache-controlpublic, max-age=31536000"
content-encodinggzip" 
content-typeapplication/json"%
dateTue, 02 Feb 2021 06:55:38 GMT"&
etagW/"6160209-7f06-5ba4f6347ae35"".
last-modifiedTue, 02 Feb 2021 00:16:54 GMT"
servernginx"
varyAccept-Encoding0�������B4https://matrixcalc.org/i18n-en.json?20210202T001643ZHPZh2`japplication/jsonrGET����������������FO��,>Q޴��$jϋ/�5��A�Eo��   ��~�      